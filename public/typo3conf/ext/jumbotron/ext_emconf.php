<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Jumbotron',
    'description' => 'Extension with custom content element',
    'category' => 'plugin',
    'author' => 'Oleg Lisovych',
    'author_email' => 'oleg.lisovych@gmail.com',
    'state' => 'alpha',
    'clearCacheOnLoad' => true,
    'version' => '0.1.0',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99',
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'MyVendor\\Jumbotron\\' => 'Classes'
        ],
    ],
];