<?php

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
    array(
        'LLL:EXT:jumbotron/Resources/Private/Language/Tca.xlf:jumbotron.title',
        'jumbotron',
        ),
    'CType',
    'jumbotron'
);

// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['jumbotron'] = array(
    'showitem' => '
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
         --palette--;;general,
         header,
         subheader,
         jumbotron_link_wizard,
         jumbotron_text,
         bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
      --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
   ',
    'columnsOverrides' => [
        'header' => [
            'config' => [
                'required' => true,
            ]
        ],
        'subheader' => [
            'config' => [
                'max' => 150,
            ]
        ],
        'bodytext' => [
            'config' => [
                'enableRichtext' => true,
                'richtextConfiguration' => 'default'
            ]
        ],
    ]
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', [
    'jumbotron_link_wizard' => [
        'label' => 'Button URL',
        'config' => [
            'type' => 'input',
            'renderType' => 'inputLink',
            'max' => 255,
        ],
    ],
    'jumbotron_text' => [
        'label' => 'Button text',
        'config' => [
            'type' => 'input',
            'max' => 30,
        ],
    ],
]);