mod.wizards.newContentElement.wizardItems.special {
    elements {
        jumbotron {
            iconIdentifier = content-image
            title = Jumbotron
            description = The jumbotron content element
            tt_content_defValues {
                CType = jumbotron
            }
        }
    }
    show := addToList(jumbotron)
}
